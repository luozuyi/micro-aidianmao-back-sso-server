package com.aidianmao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MicroAidianmaoBackSsoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroAidianmaoBackSsoServerApplication.class, args);
	}
}
